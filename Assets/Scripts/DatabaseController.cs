﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net;
using System.IO;
using UnityEngine.Networking;

[System.Serializable]
public class Instituction
{
    public int id;
    public string nome;
    public Class[] turmas;
}

[System.Serializable]
public class Class
{
    public int id;
    public int instituicao;
    public string nome;
}

[System.Serializable]
public struct FormField
{
    public string name;
    public string value;

    public FormField(string name, string value)
    {
        this.name = name;
        this.value = value;
    }
}

public class DatabaseController : MonoBehaviour
{
    public Text Text;
    public Transform ButtonsContainer;
    public Transform ClassButtonsContainer;
    public InstituctionButton ButtonPrefab;

    private Instituction[] _instituctions;

    private static DatabaseController _instance;
    public static DatabaseController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<DatabaseController>();

            return _instance;
        }
    }

	void Start ()
    {
        GetInstituctions();
        CreateInstituctionButtons();
	}

    public void PostData(string url, FormField[] fields)
    {
        StartCoroutine(Post(url, fields, () => 
        {
            GetInstituctions();
            CreateInstituctionButtons();
        }));
    }

    public void DeleteData(string uri)
    {
        StartCoroutine(Delete(uri, () =>
        {
            GetInstituctions();
            CreateInstituctionButtons();
        }));
    }

    public Instituction GetInstituctionById(int id)
    {
        foreach (var item in _instituctions)
        {
            if (item.id == id)
                return item;
        }

        return null;
    }

	private void GetInstituctions()
    {
        string jsonResponse = DatabaseRequest("https://evandroalvisi.pythonanywhere.com/instituicoes/");

        _instituctions = JsonHelper.FromJson<Instituction>(jsonResponse);
        foreach (var item in _instituctions)
        {
            item.turmas = GetClasses(item.id);
            //print(item.nome);
            //print(item.turmas[0].nome);
        }
    }
	
    private Class[] GetClasses(int instituctionId)
    {
        string jsonResponse = DatabaseRequest($"https://evandroalvisi.pythonanywhere.com/turmas/?instituicao={instituctionId}");
        return JsonHelper.FromJson<Class>(jsonResponse);
    }

    private string DatabaseRequest(string url)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();

        print(jsonResponse);

        return "{\"Items\":" + jsonResponse + "}";
    }

    private void CreateInstituctionButtons()
    {
        ClearInstituctionButtons();

        foreach (var instituction in _instituctions)
        {
            InstituctionButton button = Instantiate(ButtonPrefab, ButtonsContainer);
            button.InstituctionID = instituction.id;
            button.Text.text = $"{instituction.nome} (id: {instituction.id})";
            button.Button.onClick.AddListener(() =>
            {
                Text.text = $"Instituição\nNome: {instituction.nome}\nId: {instituction.id}\nTurmas:";
                foreach (var turma in instituction.turmas)
                {
                    Text.text += $"\n\t{turma.nome}";
                }

                ClearClassButtons();
                CreateClassButtons(GetInstituctionById(button.InstituctionID));
            });
            button.DeleteButton.URI = $"https://evandroalvisi.pythonanywhere.com/instituicoes/{instituction.id}";
        }
    }

    private void CreateClassButtons(Instituction instituction)
    {
        foreach (var turma in instituction.turmas)
        {
            InstituctionButton button = Instantiate(ButtonPrefab, ClassButtonsContainer);
            button.InstituctionID = instituction.id;
            button.Text.text = turma.nome;
            button.Button.onClick.AddListener(() =>
            {
                Text.text = $"Turma\nNome: {turma.nome}\nId: {turma.id}\nInstituição: {GetInstituctionById(turma.instituicao).nome}";
            });
            button.DeleteButton.URI = $"https://evandroalvisi.pythonanywhere.com/turmas/{turma.id}";
        }
    }

    private void ClearInstituctionButtons()
    {
        foreach (Transform item in ButtonsContainer.transform)
        {
            Destroy(item.gameObject);
        }
    }

    private void ClearClassButtons()
    {
        foreach (Transform item in ClassButtonsContainer.transform)
        {
            Destroy(item.gameObject);
        }
    }

    public void LaunchApp(string appBundle)
    {
        bool fail = false;
        string bundleId = appBundle;
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

        AndroidJavaObject launchIntent = null;
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
        }
        catch (System.Exception e)
        {
            Debug.Log(e);
            fail = true;
        }

        if (fail)
        { //open app in store
            Application.OpenURL("market://details?id="+appBundle);
        }
        else //open the app
            ca.Call("startActivity", launchIntent);

        up.Dispose();
        ca.Dispose();
        packageManager.Dispose();
        launchIntent.Dispose();
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator Post(string url, FormField[] fields, System.Action callback = null)
    {
        WWWForm form = new WWWForm();
        foreach (var field in fields)
        {
            form.AddField(field.name, field.value);
        }

        ClearClassButtons();
        ClearInstituctionButtons();

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");

                if (callback != null)
                    callback();
            }
        }
    }

    IEnumerator Delete(string uri, System.Action callback = null)
    {        
        ClearClassButtons();
        ClearInstituctionButtons();

        using (UnityWebRequest www = UnityWebRequest.Delete(uri))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form delete complete!");

                if (callback != null)
                    callback();
            }
        }
    }
}
