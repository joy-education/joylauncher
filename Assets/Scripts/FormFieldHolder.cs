﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FormFieldHolderField
{
    public string Name;
    public InputField InputField;

    public string Value
    {
        get
        {
            return InputField.text;
        }
    }
}

public class FormFieldHolder : MonoBehaviour
{
    public string URL;
    public FormFieldHolderField[] Fields;
    public Button PostButton;

    private void Start()
    {
        PostButton.onClick.AddListener(Post);
    }

    public void Post()
    {
        FormField[] fields = new FormField[Fields.Length];
        for (int i = 0; i < Fields.Length; i++)
        {
            fields[i].name = Fields[i].Name;
            fields[i].value = Fields[i].Value;
        }

        DatabaseController.Instance.PostData(URL, fields);
    }
}
