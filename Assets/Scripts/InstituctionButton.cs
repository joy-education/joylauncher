﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstituctionButton : MonoBehaviour {

    public int InstituctionID;
    public DeleteButton DeleteButton;

    private Text _text;
	public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();

            return _text;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }
}
